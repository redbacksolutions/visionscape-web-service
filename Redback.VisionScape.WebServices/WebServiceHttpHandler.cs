﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Xml;
using System.IO.Compression;
using System.IO;

namespace Redback.VisionScape.WebServices
{
    public class WebServiceHttpHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get { return true; }
        }

        public string ConnectionString { get; set; }

        public WebServiceHttpHandler()
        {
        }

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                HttpRequest request = context.Request;
                HttpResponse response = context.Response;
                ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];

                string domain = request.ServerVariables["SERVER_NAME"].ToString().ToLower();
                int siteId = MapDomainToSiteId(domain);
                string entity = GetEntity(request.Url.AbsolutePath.ToLower());
                //
                if (siteId > 0 && entity != "")
                {
                    switch (request.HttpMethod)
                    {
                        case "GET":
                            WebServiceHelperBase webServiceHelper = GetWebServiceHelper(siteId, entity, context, ConnectionString);
                            if (webServiceHelper != null)
                            {
                                webServiceHelper.ProcessGet();
                            }
                            break;
                        default:
                            Send405(context, request.HttpMethod);
                            break;
                    }
                }
                else
                {
                    Send404(context, domain + ", " + entity);
                }
            }
            catch (Exception ex)
            {
                Send500(context, ex.Message);
            }
            context.ApplicationInstance.CompleteRequest();
        }

        private WebServiceHelperBase GetWebServiceHelper(int siteId, string entity, HttpContext context, string connectionString)
        {
            //
            //TODO: Move this to a database table
            //
            if (siteId == 887 && entity == "video")
                return new ProfMontageWebServiceHelper(siteId, entity, context, connectionString);
            else
                return null;
        }

        private string GetEntity(string path)
        {
            const string prefix = "/webservices/";
            int pos = path.IndexOf(prefix);
            if (pos >= 0)
                return path.Substring(pos + prefix.Length);
            else
                return "";
        }

        private int MapDomainToSiteId(string domain)
        {
            // For debugging in development environment
            if (domain == "localhost") return 887;

            // URL must start with "HTTP://WWW."
            string siteUrl;
            domain = domain.ToLower();
            if (domain.StartsWith("https://wwww."))
                siteUrl = domain;
            else if (domain.StartsWith("http://wwww."))
                siteUrl = domain;
            else if (domain.StartsWith("www."))
                siteUrl = "http://" + domain;
            else
                siteUrl = "http://wwww." + domain;

            string sqlString = @"
SELECT SiteId
  FROM [tbl_sites]
 WHERE SiteUrl = @siteUrl
";
            SqlConnection connection = new SqlConnection(ConnectionString);
            connection.Open();

            SqlCommand command = new SqlCommand(sqlString, connection);
            command.Parameters.AddWithValue("@siteUrl", siteUrl);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataSet dataSet = new DataSet();
            adapter.Fill(dataSet);
            if (dataSet.Tables[0].Rows.Count == 1)
                return (int)dataSet.Tables[0].Rows[0]["SiteId"];
            else
                return -1;
        }

        public static void SendHtml(HttpContext context, string content, int statusCode)
        {
            HttpResponse response = context.Response;
            response.ContentType = "text/html";
            response.StatusCode = statusCode;
            response.Cache.SetCacheability(HttpCacheability.NoCache);
            if (CanCompressResponse(context))
            {
                response.AddHeader("Content-Encoding", "gzip");
                byte[] buf = System.Text.Encoding.ASCII.GetBytes(content);
                //
                response.BinaryWrite(Compress(buf));
            }
            else
            {
                response.Write(content);
            }
            context.ApplicationInstance.CompleteRequest();
        }

        public static void Send403(HttpContext context)
        {
            SendHtml(context, "<html><body><h1>Not Authorised</h1></body></html>", 403);
        }

        public static void Send404(HttpContext context, string name)
        {
            SendHtml(context, string.Format("<html><body><h1>Not Found: {0}</h1></body></html>", HttpUtility.HtmlEncode(name)), 404);
        }

        public static void Send405(HttpContext context, string name)
        {
            SendHtml(context, string.Format("<html><body><h1>Method Not Allowed: {0}</h1></body></html>", HttpUtility.HtmlEncode(name)), 404);
        }

        public static void Send500(HttpContext httpContext, string message)
        {
            SendHtml(httpContext, "<html><body><h1>An error occured sorry.</h1><p>" + message + "</p></body></html>", 500);
        }

        public static void SendXml(HttpContext httpContext, XmlDocument content)
        {
            SendXml(httpContext, content, 200);
        }

        public static void SendXml(HttpContext context, XmlDocument content, int statusCode)
        {
            HttpResponse response = context.Response;
            response.ContentType = "text/xml";
            response.StatusCode = statusCode;
            response.Cache.SetCacheability(HttpCacheability.NoCache);
            if (CanCompressResponse(context))
            {
                response.AddHeader("Content-Encoding", "gzip");
                GZipStream gzipStream = new GZipStream(response.OutputStream, CompressionMode.Compress);
                content.Save(gzipStream);
                gzipStream.Close();
            }
            else
            {
                content.Save(response.OutputStream);
            }
        }

        public static bool CanCompressResponse(HttpContext context)
        {
            string acceptEncoding = context.Request.Headers["Accept-Encoding"];
            if (acceptEncoding != null && (acceptEncoding.ToLower().Contains("gzip") || acceptEncoding.ToLower().Contains("deflate")))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static byte[] Compress(byte[] Buffer)
        {
            MemoryStream ms = new MemoryStream();
            GZipStream gzipStream = new GZipStream(ms, CompressionMode.Compress);
            gzipStream.Write(Buffer, 0, Buffer.Length);
            gzipStream.Close();
            byte[] Result = ms.ToArray();
            ms.Close();
            return Result;
        }
        
        public static DateTime QueryStringToDateTime(HttpContext context, string name, DateTime defaultValue)
        {
            DateTime result;
            string since = context.Request.QueryString[name];
            if (since == null || !DateTime.TryParse(since.Replace("_", ":"), out result))
            {
                result = defaultValue;
            }
            return result;
        }
    }
}
