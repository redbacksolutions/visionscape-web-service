﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Xml;

namespace Redback.VisionScape.WebServices
{
    public class ProfMontageWebServiceHelper : WebServiceHelperBase
    {
        public ProfMontageWebServiceHelper(int siteId, string entity, HttpContext context, string connectionString)
            : base(siteId, entity, context, connectionString)
        {
        }

        public override void ProcessGet()
        {
            switch (Entity.ToLower())
            {
                case "video":
                    ProcessGetVideo();
                    break;
                default:
                    WebServiceHttpHandler.Send404(Context, Entity);
                    break;
            }
        }

        private void ProcessGetVideo()
        {
            DateTime sinceDateTime = WebServiceHttpHandler.QueryStringToDateTime(Context, "since", DateTime.Parse("1 Jan 1900"));
            //
            DataSet dataSet = GetVideosForSync(SiteId, "Video", "Home", sinceDateTime);
            //
            XmlDocument xmlDocument = new XmlDocument();
            XmlElement root = xmlDocument.CreateElement("SearchResult");
            xmlDocument.AppendChild(root);
            //
            foreach (DataRow dataRow in dataSet.Tables[0].Rows)
            {
                try
                {
                    XmlElement result = MapHtmlToXml(xmlDocument, (string)dataRow["html"], "<ul id=\"webServiceContent\"", "</ul>");
                    if (result != null)
                    {
                        root.AppendChild(result);
                    }
                }
                catch (Exception ex)
                {
                }
            }
            WebServiceHttpHandler.SendXml(Context, xmlDocument);
        }

        private DataSet GetVideosForSync(int siteId, string pageTypeName, string componentTitle, DateTime lastUpdated)
        {
            string sqlString = @"
  SELECT   P.pageName
       ,   P.pageFriendlyUrlName
       , CHB.Html
       , CHB.dateUpdated
    FROM [tbl_Sites]					S
    JOIN [tbl_pages]					P	ON P.SiteId = S.SiteId
    JOIN [tbl_pages_type]				PT	ON PT.pageTypeId = P.pageTypeId
    JOIN [tbl_compPageParser]			CPP	ON CPP.PageId = P.PageId
    JOIN [tbl_components]				C	ON C.ComponentId = CPP.ComponentId
    JOIN [tbl_components_HtmlBlock]	    CHB	ON CHB.ComponentId = CPP.ComponentId
   WHERE   S.SiteId = @SiteId
     AND  PT.PageTypeName = @PageTypeName
     AND   C.componentTitle = @ComponentTitle
     AND CHB.Live = 1
     AND CHB.dateUpdated > @dateUpdated
ORDER BY CHB.dateUpdated DESC
";

            SqlConnection connection = new SqlConnection(ConnectionString);
            connection.Open();

            SqlCommand command = new SqlCommand(sqlString, connection);
            command.Parameters.AddWithValue("@SiteId", siteId);
            command.Parameters.AddWithValue("@PageTypeName", pageTypeName);
            command.Parameters.AddWithValue("@ComponentTitle", componentTitle);
            command.Parameters.AddWithValue("@DateUpdated", lastUpdated);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataSet dataSet = new DataSet();
            adapter.Fill(dataSet);
            return dataSet;
        }

        private static XmlElement MapHtmlToXml(XmlDocument xmlDocument, string html, string start, string end)
        {
            List<KeyValuePair<string, string>> pairs = GetKeyValuePairs(html, start, end);
            if (pairs.Count > 0)
            {
                XmlElement videoElement = xmlDocument.CreateElement("Video");
                foreach (var pair in pairs)
                {
                    try
                    {
                        XmlAttribute attribute = xmlDocument.CreateAttribute(pair.Key);
                        attribute.Value = pair.Value;
                        videoElement.Attributes.Append(attribute);
                    }
                    catch (Exception ex)
                    {
                    }
                }
                return videoElement;
            }
            return null;
        }

        private static List<KeyValuePair<string, string>> GetKeyValuePairs(string html, string start, string end)
        {
            List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();
            try
            {
                XmlDocument xmlDocument = ExtractBetween(html, start, end);
                foreach (XmlElement element in xmlDocument.SelectNodes("//li"))
                {
                    string[] bits = element.InnerText.Split(new char[] { ':' }, 2);
                    if (bits.Length > 0)
                    {
                        result.Add(new KeyValuePair<string, string>(bits[0].Trim(), bits[1].Trim()));
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        private static XmlDocument ExtractBetween(string imput, string start, string end)
        {
            int pos = imput.ToLower().IndexOf(start.ToLower());
            if (pos > 0)
            {
                imput = imput.Substring(pos);
                pos = imput.ToLower().IndexOf(end.ToLower());
                if (pos > 0)
                {
                    imput = imput.Substring(0, pos + end.Length);
                    //
                    XmlDocument xml = new XmlDocument();
                    xml.LoadXml(imput);
                    //
                    return xml;
                }
            }
            return null;
        }
    }
}
