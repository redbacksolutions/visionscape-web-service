﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Redback.VisionScape.WebServices
{
    public abstract class WebServiceHelperBase
    {
        public WebServiceHelperBase(int siteId, string entity, HttpContext context, string connectionString)
        {
            SiteId = siteId;
            Entity = entity;
            Context = context;
            ConnectionString = connectionString;
        }

        public int SiteId { get; private set; }

        public string Entity { get; private set; }

        public HttpContext Context { get; private set; }

        public string ConnectionString { get; private set; }

        public abstract void ProcessGet();
    }
}
